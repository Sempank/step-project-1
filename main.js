$(document).ready(function () {

  /* FIRST SECTION */


  $(".servise-menu .servise-menu-item").click(function () {
    $(this)
      .addClass("servise-menu-active")
      .siblings()
      .removeClass("servise-menu-active")
      .closest(".servises")
      .find(".servises-description")
      .removeClass("active")
      .eq($(this).index())
      .addClass("active")
  });


  /*SECOND WORK SECTION*/


  $(".work-menu-item").click(function () {
    $(this)
      .addClass("work-menu-active-item")
      .siblings()
      .removeClass("work-menu-active-item")
  });

  $(".work-menu-item").click(function () {
    let className = $(this).attr("data-target");
    $(".card").hide(1000);
    $(`.${className}`).show(500);
    if ($(this).attr("data-target") != ("work-image-link")) {
      $(".load-img-btn").hide()
    }
  });


  $(".work-image-link").hide();
  $(".work-image-link").slice(0, 12).show();
  $(".load-img-btn").click(function () {
    $(".work-image-link:hidden").slice(0, 12).show();
    if (!$(".work-image-link").is(":hidden")) {
      $(this).hide()
    }
  })



  /* SLIDER */


  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,

    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  });
  $('.slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    centerPadding: 0,
    focusOnSelect: true,
    centerMode: true,
    asNavFor: '.slider-for'

  });
  $('.slider-for').on('afterChange', function (event, slick, currentSlide, nextSlide) {
    $('.person-text').removeClass('active').eq(currentSlide).addClass('active');
    $('.person-name').removeClass('active').eq(currentSlide).addClass('active');
    $('.person-profession').removeClass('active').eq(currentSlide).addClass('active');
    $('.person-photo-nav').eq(nextSlide).addClass('active');


  });
});